export const getData = async (url) => {
  const res = await fetch(url);
  if (!res.ok) {
      throw new Error(`Received ${res.status}`);
  }
  return await res.json();
};

export const getIndex = (items, id) => {
  return items.findIndex((item) => item.id === id);
};
