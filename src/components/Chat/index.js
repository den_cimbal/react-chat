import React, { Component } from 'react';
import Header from "../Header";
import MessageList from "../MessageList";
import MessageInput from "../MessageInput";
import Preloader from "../Preloader";
import { getData, getIndex } from '../../helpers/helpers';
import './chat.css';
let messageId = 0;
class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      messages: [],
      currentUser: {
        user: 'Denis',
        userId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce5'
      },
      editMessage: { isEdit: false, id: null, text: null }
    }
  }

  componentDidMount() {
    getData(this.props.url)
      .then((data) => {
        const messages = data.map((message) => {
          message.isLiked = false;
          message.date = new Date(message.createdAt).toLocaleString('en-GB', {
            day: 'numeric',
            month: 'long',
            weekday: 'long',
          });
          message.time = new Date(message.createdAt).toString().substring(16, 21);
          message.dateTime = new Date(message.createdAt).toLocaleString('en-GB', {
            day: 'numeric',
            year: 'numeric',
            month: 'numeric'
          }).replace(/\//g, '.') + ' ' + message.time;
          message.dateDivider = new Date(message.createdAt).toLocaleString('en-US', {
            day: 'numeric',
            year: 'numeric',
            month: 'numeric'
          });
          return message;
        });

        this.setState({
          messages: messages,
          isLoading: false
        })
      });
  }

  createMessage = (text, user) => {
    const id = messageId++;
    const isoDate = new Date().toISOString();
    const date = new Date(isoDate).toLocaleString('en-GB', {
      day: 'numeric',
      month: 'long',
      weekday: 'long',
    });;
    const time = new Date(isoDate).toString().substring(16, 21);
    const dateTime = new Date(isoDate).toLocaleString('en-GB', {
      day: 'numeric',
      year: 'numeric',
      month: 'numeric'
    }).replace(/\//g, '.') + ' ' + time;

    const dateDivider = new Date(isoDate).toLocaleString('en-US', {
      day: 'numeric',
      year: 'numeric',
      month: 'numeric'
    });

    return { id, text, date, time, dateTime, dateDivider, ...user };
  }

  handleMessageLike = (id) => {
    this.setState((prevState) => {
      const messageIndex = getIndex(prevState.messages, id);
      const oldMessage = prevState.messages[messageIndex];
      const newLikeValue = !oldMessage.isLiked;
      const updatedMessage = { ...oldMessage, isLiked: newLikeValue };
      const messages = [
        ...prevState.messages.slice(0, messageIndex),
        updatedMessage,
        ...prevState.messages.slice(messageIndex + 1)
      ];
      return { messages };
    });
  }

  handleMessageDelete = id => {
    this.setState(prevState => {
      const messageIndex = getIndex(prevState.messages, id);
      const messages = [...prevState.messages.slice(0, messageIndex), ...prevState.messages.slice(messageIndex + 1)];
      return { messages };
    });
  }

  handleMessageEdit = ({ id, text }) => {
    this.setState({ editMessage: { isEdit: true, text, id } });
  }

  handleMessageAdd = (text) => {
    this.setState((prevState) => {
      const newMessage = this.createMessage(text, prevState.currentUser);
      const messages = [...prevState.messages, { ...newMessage }];
      return { messages }
    });
  }

  onMessageEdit = ({ isEdit, text, id }) => {
    this.setState((prevState) => {
      if (!isEdit) {
        return { editMessage: { isEdit: false, text: null, id: null } }
      }

      const messageIndex = getIndex(prevState.messages, id);
      const oldMessage = prevState.messages[messageIndex];
      const updatedMessage = { ...oldMessage, text };
      const messages = [
        ...prevState.messages.slice(0, messageIndex),
        updatedMessage,
        ...prevState.messages.slice(messageIndex + 1)
      ];

      return { messages, editMessage: { isEdit: false, text: null, id: null } }
    });
  };

  render() {
    const { isLoading, messages, currentUser } = this.state;
    const headerTitle = 'React Chat';
    const participantsNumber = new Set(messages.map((message) => message.user)).size;
    const messagesNumber = messages.length;
    const lastMessage = messages.length && messages.slice(-1)[0].dateTime;

    return (
      <div className="chat">
        {
          isLoading
            ? <Preloader />
            : <>
                <Header
                  title={ headerTitle }
                  participants={ participantsNumber }
                  messages={ messagesNumber }
                  lastMessage={ lastMessage }
                />

                <MessageList
                  currentUserId={ currentUser.userId }
                  messages={ messages }
                  onMessageLike={ this.handleMessageLike }
                  onMessageDelete={ this.handleMessageDelete }
                  onMessageEdit={ this.handleMessageEdit }
                />

                <MessageInput
                  onMessageAdd={ this.handleMessageAdd }
                  onMessageEdit={ this.onMessageEdit }
                  editMessage={ this.state.editMessage }
                />
              </>
        }
      </div>
    );
  }
}

export default Chat;