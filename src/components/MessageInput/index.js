import React, { Component } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandPointRight, faTimes } from "@fortawesome/free-solid-svg-icons";
import './message-input.css';

class MessageInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      editedMessage: { isEdit: false, id: '' }
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.editMessage !== this.props.editMessage) {
      const { isEdit, id, text } = this.props.editMessage;
      this.setState({
        value: text,
        editedMessage: { isEdit, id }
      });
    }
  }

  handleChange = ({ target }) => {
    const value = target.value;
    this.setState({ value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { editedMessage: { isEdit, id }, value } = this.state;

    if (isEdit) {
      const { onMessageEdit } = this.props;

      if (!value) {
        return this.cancelEditMessage(onMessageEdit);
      } else {
        return this.updateMessage(onMessageEdit, id, value);
      }
    }

    const { onMessageAdd } = this.props;
    this.addMessage(onMessageAdd, value);
  };

  addMessage(onMessageAdd, value) {
    if (!value) {
      return
    }

    onMessageAdd(value);
    this.setState({
      value: ''
    });
  }

  updateMessage(onMessageEdit, id, value) {
    const editedMessage = { isEdit: true, id, text: value };
    onMessageEdit(editedMessage);
    this.setState({
      value: '',
      editedMessage: { isEdit: false, id: '' }
    });
  }

  cancelEditMessage(onMessageEdit) {
    onMessageEdit({ isEdit: false });
    this.setState({
      value: '',
      editedMessage: { isEdit: false, id: '' }
    });
  }

  render() {
    const { onMessageEdit } = this.props;
    const {  editedMessage: { isEdit } } = this.state;

    return (
      <div className="message-input">
        <textarea
          className="message-input-text"
          onChange={ this.handleChange }
          value={ this.state.value || '' }
          placeholder="Message"
        ></textarea>
        <div className={`message-input-container ${isEdit ? 'message-input-container-edit' : ''}`} >
          <button className="message-input-button" onClick={ this.handleSubmit }><FontAwesomeIcon icon={ faHandPointRight } /></button>
          {this.state.editedMessage.isEdit && (
            <button className="message-input-cancel" onClick={ () => this.cancelEditMessage(onMessageEdit) }><FontAwesomeIcon icon={ faTimes } /></button>
          )}
        </div>
      </div>
    );
  }
}

export default MessageInput;