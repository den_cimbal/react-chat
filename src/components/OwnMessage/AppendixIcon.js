import React, { Component } from 'react';

class AppendixIcon extends Component {
  render() {
    return (
      <svg width="9" height="20" xmlns="http://www.w3.org/2000/svg">
        <path d="M6 17H0V0c.193 2.84.876 5.767 2.05 8.782.904 2.325 2.446 4.485 4.625 6.48A1 1 0 016 17z" className="corner"></path>
      </svg>
    )
  }
}

export default AppendixIcon;