import React, { Component } from 'react';
import './preloader.css';

class Preloader extends Component {
  render() {
    return (
      <div className="preloader"></div>
    );
  }
}

export default Preloader;