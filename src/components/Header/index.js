import React, { Component } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUsers, faComments, faHourglass } from "@fortawesome/free-solid-svg-icons";
import './header.css';

class Header extends Component {
  render() {
    const props = this.props;

    return (
      <div className="header">
        <h3 className="header-title">{ props.title }</h3>
        <div className="header-info"><FontAwesomeIcon icon={ faUsers } /> <span className="header-users-count">{ props.participants }</span> participants</div>
        <div className="header-info"><FontAwesomeIcon icon={ faComments } /> <span className="header-messages-count">{ props.messages }</span> messages</div>
        <div className="header-info"><FontAwesomeIcon icon={ faHourglass } /> Last message at <span className="header-last-message-date">{ props.lastMessage }</span></div>
      </div>
    );
  }
}

export default Header;