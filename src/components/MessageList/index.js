import React, { Component } from 'react';
import Message from '../Message';
import OwnMessage from '../OwnMessage';
import './message-list.css';

class MessageList extends Component {
  messagesEndRef = React.createRef();

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate(prevProps) {
    const { messages: currentMessages } = this.props;
    const { messages } = prevProps;
    if (currentMessages.length > messages.length) {
      this.scrollToBottom();
    }
  }

  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  }

  showDate = (date, oldDate) => {
    const today = new Date();
    const dateToCheck = new Date(date);
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);

    if(dateToCheck.getDate() === today.getDate() && dateToCheck.getMonth() === today.getMonth() && dateToCheck.getFullYear() === today.getFullYear()) {
      return 'Today';
    } else if(dateToCheck.getDate() === yesterday.getDate() && dateToCheck.getMonth() === yesterday.getMonth() && dateToCheck.getFullYear() === yesterday.getFullYear()) {
      return 'Yesterday';
    } else  {
      return oldDate;
    }
  }

  render() {
    const { currentUserId, messages, onMessageLike, onMessageEdit, onMessageDelete } = this.props;
    let date;
    const messageItems = messages.map(message => {
      const { id, ...messageProps } = message;
      const isSameDate = date === messageProps.date;
      if (!isSameDate) {
        date = messageProps.date;
      }

      const line = <div key={ messageProps.date } className="messages-divider"><span>{ this.showDate(messageProps.dateDivider, messageProps.date) }</span></div>;

      return (
        <React.Fragment key={ id }>
          { !isSameDate && line }

          {
            currentUserId === message.userId
              ? <OwnMessage
                  {...messageProps}
                  onDelete={ () => onMessageDelete(id) }
                  onEdit={ () => onMessageEdit({ id, text: messageProps.text }) }
                />
              : <Message
                  { ...messageProps }
                  onLike={ () => onMessageLike(id) }
                />
          }
        </React.Fragment>
      );
    });

    return (
      <div className="message-list">
        { messageItems }
        <div ref={this.messagesEndRef}></div>
      </div>
    );
  }
}

export default MessageList;